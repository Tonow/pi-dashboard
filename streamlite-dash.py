import streamlit as st
from random import randrange
from PIL import Image
import glob
import pandas as pd
import numpy as np
from GPSPhoto import gpsphoto
from config import PATHS

st.set_page_config(
    page_title="Ex-stream-ly Cool App",
    page_icon="🧊",
    layout="wide",
    initial_sidebar_state="expanded",
)




def get_tmp_lac():
    from bs4 import BeautifulSoup as bs
    import urllib.request
    import requests

    # Request the page
    url = 'https://romma.fr/station_24.php?id=153&tempe1'

    page = requests.get(url)
    page=urllib.request.urlopen(url,timeout=5)

    spans= bs(page).find_all('span', {'class': 'mediumTexte'})
    res=None
    for span in spans:
        span=span.text
        span_strip = span.rstrip()
        if span[0].isnumeric() and span_strip.endswith('°C'):
            res = span_strip
    res

def get_image():
    images = []
    for path in PATHS:
        for image in glob.glob(path, recursive=True):
            images.append(image)
    image_random_place = randrange(len(images))
    image_path = images[image_random_place]
    image = Image.open(images[image_random_place])
    return image, image_path

def get_lat_long(image_path):
    data = gpsphoto.getGPSData(image_path)
    dfmap = None
    if data.get('Latitude') and data.get('Longitude'):
        # df = pd.DataFrame(
        #     np.array(float(data['Latitude']), float(data['Longitude'])),
        #     columns=['lat', 'lon'])

        dfmap = pd.DataFrame({
            'lat' : [data['Latitude']],
            'lon' : [data['Longitude']]
        })
    return dfmap

def img_infos(image_path):
    data = gpsphoto.getGPSData(image_path)
    df = None
    if data.get('Latitude') and data.get('Longitude'):
        df = pd.DataFrame({
            'lat' : [data['Latitude']],
            'date' : [data['Date']],
            'lon' : [data['Longitude']]
        })
    return df



col1, col2 = st.columns([10, 2])

with col1:
    image, image_path = get_image()
    st.image(image, caption=f'Image {image_path}')
    st.write(img_infos(image_path))
    st.map(get_lat_long(image_path))




with col2:
    if st.button("Changer l'image"):
        pass
    st.markdown(f'# Temperature du lac:')
    # tmp = f'{get_tmp_lac()}'
    st.write(get_tmp_lac())
    st.markdown(f"### [site grand lac](https://romma.fr/station_24.php?id=153&tempe1)")
